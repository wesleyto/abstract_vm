// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   operand.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/30 21:28:00 by wto               #+#    #+#             //
//   Updated: 2018/10/30 21:28:01 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "operand.hpp"

Operand::Operand(eOperandType type, std::string value)
{
	this->_value = value;
	this->_type = type;
	return;
}

Operand::Operand(Operand const &other)
{
	*this = other;
	return;
}

Operand				&Operand::operator=(Operand const &other)
{
	this->_value = other.toString();
	this->_type = other.getType();
	return (*this);
}

int					Operand::getPrecision(void) const
{
	return this->getType();
}

eOperandType		Operand::getType(void) const
{
	return (this->_type);
}

IOperand const		*Operand::operator+(IOperand const &other) const
{
	std::string s;
	eOperandType newType = this->_type > other.getType() ? this->_type : other.getType();
	if (newType == dbl)
		s = std::to_string((double)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) + (double)(*const_cast<Operand *>(this)));
	else if (newType == flt)
		s = std::to_string((float)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) + (float)(*const_cast<Operand *>(this)));
	else if (newType == int32)
		s = std::to_string((int)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) + (int)(*const_cast<Operand *>(this)));
	else if (newType == int16)
		s = std::to_string((short)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) + (short)(*const_cast<Operand *>(this)));
	else
		s = std::to_string((char)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) + (char)(*const_cast<Operand *>(this)));
	return (new Operand(newType, s));
}

IOperand const		*Operand::operator-(IOperand const &other) const
{
	std::string s;
	eOperandType newType = this->_type > other.getType() ? this->_type : other.getType();
	if (newType == dbl)
		s = std::to_string((double)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) - (double)(*const_cast<Operand *>(this)));
	else if (newType == flt)
		s = std::to_string((float)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) - (float)(*const_cast<Operand *>(this)));
	else if (newType == int32)
		s = std::to_string((int)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) - (int)(*const_cast<Operand *>(this)));
	else if (newType == int16)
		s = std::to_string((short)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) - (short)(*const_cast<Operand *>(this)));
	else
		s = std::to_string((char)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) - (char)(*const_cast<Operand *>(this)));
	return (new Operand(newType, s));
}

IOperand const		*Operand::operator*(IOperand const &other) const
{
	std::string s;
	eOperandType newType = this->_type > other.getType() ? this->_type : other.getType();
	if (newType == dbl)
		s = std::to_string((double)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) * (double)(*const_cast<Operand *>(this)));
	else if (newType == flt)
		s = std::to_string((float)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) * (float)(*const_cast<Operand *>(this)));
	else if (newType == int32)
		s = std::to_string((int)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) * (int)(*const_cast<Operand *>(this)));
	else if (newType == int16)
		s = std::to_string((short)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) * (short)(*const_cast<Operand *>(this)));
	else
		s = std::to_string((char)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) * (char)(*const_cast<Operand *>(this)));
	return (new Operand(newType, s));
}

IOperand const		*Operand::operator/(IOperand const &other) const
{
	std::string s;
	eOperandType newType = this->_type > other.getType() ? this->_type : other.getType();
	if (newType == dbl)
		s = std::to_string((double)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) / (double)(*const_cast<Operand *>(this)));
	else if (newType == flt)
		s = std::to_string((float)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) / (float)(*const_cast<Operand *>(this)));
	else if (newType == int32)
		s = std::to_string((int)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) / (int)(*const_cast<Operand *>(this)));
	else if (newType == int16)
		s = std::to_string((short)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) / (short)(*const_cast<Operand *>(this)));
	else
		s = std::to_string((char)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) / (char)(*const_cast<Operand *>(this)));
	return (new Operand(newType, s));
}

IOperand const		*Operand::operator%(IOperand const &other) const
{
	std::string s;
	eOperandType oType = other.getType();
	if (oType != dbl && oType != flt)
	{
		eOperandType newType = this->_type > other.getType() ? this->_type : other.getType();
		if (newType == int32)
			s = std::to_string((int)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) % (int)(*const_cast<Operand *>(this)));
		else if (newType == int16)
			s = std::to_string((short)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) % (short)(*const_cast<Operand *>(this)));
		else
			s = std::to_string((char)(*const_cast<Operand *>(reinterpret_cast<const Operand *>(&other))) % (char)(*const_cast<Operand *>(this)));
		return (new Operand(newType, s));
	}
	return ((const IOperand *)this);
}

std::string const	&Operand::toString(void) const
{
	return (this->_value);
}

Operand::operator	double(void) const
{
	return (std::stod(this->_value));
}

Operand::operator	float(void) const
{
	return (std::stof(this->_value));
}

Operand::operator	int(void) const
{
	return (std::stoi(this->_value));
}

Operand::operator	short(void) const
{
	return ((short)(std::stoi(this->_value)));
}

Operand::operator	char(void) const
{
	return ((char)(std::stoi(this->_value)));
}
