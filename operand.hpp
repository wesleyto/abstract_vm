// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   operand.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/30 21:28:04 by wto               #+#    #+#             //
//   Updated: 2018/10/30 21:28:04 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OPERAND_H
# define OPERAND_H

#include "IOperand.hpp"

class Operand : public IOperand{

public:
	Operand(void);
	Operand(eOperandType type, std::string value);
	Operand(Operand const &other);
	virtual ~Operand(void) {};
	Operand &operator=(Operand const &other);

	int					getPrecision(void) const;
	eOperandType		getType(void) const;

	IOperand const		*operator+(IOperand const &other) const;
	IOperand const		*operator-(IOperand const &other) const;
	IOperand const		*operator*(IOperand const &other) const;
	IOperand const		*operator/(IOperand const &other) const;
	IOperand const		*operator%(IOperand const &other) const;

	operator			double(void) const;
	operator			float(void) const;
	operator			int(void) const;
	operator			short(void) const;
	operator			char(void) const;

	std::string const	&toString(void) const;

private:
	std::string			_value;
	eOperandType 		_type;
};

#endif
