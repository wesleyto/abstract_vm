// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   abstract_vm.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/26 23:11:45 by wto               #+#    #+#             //
//   Updated: 2018/10/26 23:11:45 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "abstract_vm.hpp"

AbstractVM::AbstractVM(void)
{
	return;
}

AbstractVM::AbstractVM(AbstractVM const &other)
{
	*this = other;
}

AbstractVM &AbstractVM::operator=(AbstractVM const &other)
{
	(void)other;
	return (*this);
}

IOperand const * AbstractVM::createOperand( eOperandType type, std::string const & value ) const
{
	IOperand const * (AbstractVM::*fptrs[])(std::string const & value) const = {
		&AbstractVM::createInt8,
		&AbstractVM::createInt16,
		&AbstractVM::createInt32,
		&AbstractVM::createFloat,
		&AbstractVM::createDouble
	};
	return ((this->*fptrs[type])(value));
}

IOperand const * AbstractVM::createInt8( std::string const & value ) const
{
	return (reinterpret_cast<IOperand const *>(new Operand(int8, value)));
}

IOperand const * AbstractVM::createInt16( std::string const & value ) const
{
	return (reinterpret_cast<IOperand const *>(new Operand(int16, value)));
}

IOperand const * AbstractVM::createInt32( std::string const & value ) const
{
	return (reinterpret_cast<IOperand const *>(new Operand(int32, value)));
}

IOperand const * AbstractVM::createFloat( std::string const & value ) const
{
	return (reinterpret_cast<IOperand const *>(new Operand(flt, value)));
}

IOperand const * AbstractVM::createDouble( std::string const & value ) const
{
	return (reinterpret_cast<IOperand const *>(new Operand(dbl, value)));
}

int main(void)
{
	AbstractVM *a = new AbstractVM();
	Operand *op = const_cast<Operand *>(
		reinterpret_cast<Operand const *>(
			a->createOperand(int8, "64")
			)
		);
	Operand *other = const_cast<Operand *>(
		reinterpret_cast<Operand const *>(
			a->createOperand(int8, "64")
			)
		);
	op = reinterpret_cast<Operand *>(
		const_cast<IOperand *>(*op * *other)
		);
	std::cout << op->toString() << std::endl;
	return (0);
}
