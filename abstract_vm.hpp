// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   abstract_vm.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/26 23:12:35 by wto               #+#    #+#             //
//   Updated: 2018/10/26 23:12:36 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ABSTRACT_VM_H
# define ABSTRACT_VM_H

#include "operand.hpp"
#include <iostream>

class AbstractVM {

public:
	AbstractVM(void);
	AbstractVM(AbstractVM const &other);
	~AbstractVM(void);
	AbstractVM &operator=(AbstractVM const &other);
	IOperand const * createOperand( eOperandType type, std::string const & value ) const;

private:
	IOperand const * createInt8( std::string const & value ) const;
	IOperand const * createInt16( std::string const & value ) const;
	IOperand const * createInt32( std::string const & value ) const;
	IOperand const * createFloat( std::string const & value ) const;
	IOperand const * createDouble( std::string const & value ) const;
};

#endif
