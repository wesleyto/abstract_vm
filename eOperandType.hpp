// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   eOperandType.hpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/10/26 23:35:11 by wto               #+#    #+#             //
//   Updated: 2018/10/26 23:35:11 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef EOPERANDTYPE_H
# define EOPERANDTYPE_H

enum eOperandType { int8, int16, int32, flt, dbl };

#endif
