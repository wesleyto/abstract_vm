#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/26 23:08:10 by wto               #+#    #+#              #
#    Updated: 2018/10/26 23:08:11 by wto              ###   ########.fr        #
#                                                                              #
#******************************************************************************#


NAME		=	abstract_vm
SRC			=	abstract_vm operand
CSRC		=	$(addsuffix .cpp, $(SRC))
HSRC		=	$(addsuffix .hpp, $(SRC))
OSRC		=	$(addsuffix .o, $(SRC))
GCHSRC		=	$(addsuffix .gch, $(HSRC))
CFLAGS		=	-Wall -Wextra -Werror -std=c++98 -pedantic
RM			=	/bin/rm
COMPILER	=	clang++

all: $(NAME)

$(NAME):
	$(COMPILER) $(CFLAGS) -c $(CSRC) $(HSRC)
	$(COMPILER) $(CFLAGS) $(OSRC) -o $(NAME)

clean:
	$(RM) -f $(GCHSRC) $(OSRC)

fclean: clean
	$(RM) -f $(NAME)

re: fclean all
